/*
 * ATF Loader for Tegra X1
 *
 * Copyright (c) 2020 langerhans
 * Copyright (c) 2020-2021 CTCaer
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms and conditions of the GNU General Public License,
 * version 2, as published by the Free Software Foundation.
 *
 * This program is distributed in the hope it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>

#include "atf.h"
#include <display/di.h>
#include <mem/mc_t210.h>
#include <mem/minerva.h>
#include <soc/bpmp.h>
#include <soc/clock.h>
#include <soc/ccplex.h>
#include <soc/fuse.h>
#include <soc/hw_init.h>
#include <soc/pmc_lp0_t210.h>
#include <soc/pmc.h>
#include <soc/t210.h>
#include <storage/nx_sd.h>
#include <utils/btn.h>
#include <utils/util.h>
#include <utils/types.h>

#include "config.h"

extern hekate_config h_cfg;

// Enable debug printing on hekatf.
#define DEBUG_HEKATF

#ifdef DEBUG_HEKATF
	#include <gfx_utils.h>
	#define DPRINTF(...) gfx_printf(__VA_ARGS__)
#else
	#define DPRINTF(...)
#endif

// Configurable carveout enable config.
#define CARVEOUT_NVDEC_ENABLE  0 // L4T: Enabled. On 4GB+ models with full TOS.
#define CARVEOUT_GENFW_ENABLE  1 // Custom carveout for general firmware.
#define CARVEOUT_TSEC_ENABLE   0 // L4T: Enabled. On 4GB+ models with full TOS.

#if CARVEOUT_NVDEC_ENABLE && CARVEOUT_GENFW_ENABLE
#error "NVDEC and GENFW carveouts can't be used together!"
#endif

// Carveout sizes.
#define CARVEOUT_NVDEC_SIZE  SZ_1M
#define CARVEOUT_GPUFW_SIZE  SZ_256K
#define CARVEOUT_GPUWPR_SIZE (SZ_512K + SZ_128K)
#define CARVEOUT_TSEC_SIZE   SZ_1M
#define CARVEOUT_GENFW_SIZE  SZ_128K

#define ACR_GSC3_ENABLE_MAGIC 0xC0EDBBCC

// TZDRAM addresses and sizes.
#define TZDRAM_SIZE (SZ_1M + SZ_4M) // 1MB Secure Element + 4MB TTB.
#define TZDRAM_BASE (0xFFFFFFFF - TZDRAM_SIZE + 1)  // 0xFFB00000.

// TZDRAM addresses and sizes after Secure Elements.
#define TZDRAM_ACTUAL_BASE    (TZDRAM_BASE - SZ_1M) // 0xFFA00000.
#define TZDRAM_ACTUAL_SIZE    (TZDRAM_SIZE + SZ_1M)

#define SC7ENTRY_HDR_SIZE 0x400

// Additional Secure Elements addresses.
#define SC7EXIT_HDR_BASE (TZDRAM_ACTUAL_BASE + 0)                   // 1MB before TZDRAM_BASE
#define SC7ENTRY_BASE 	 (TZDRAM_ACTUAL_BASE + SC7ENTRY_HDR_SIZE)   // No header.
#define SC7EXIT_BASE 	 (TZDRAM_ACTUAL_BASE + SZ_512K)             // 512KB before TZDRAM_BASE
#define PAYLOAD_BASE 	 (TZDRAM_ACTUAL_BASE + SZ_1M - SZ_256K)     // 256KB before TZDRAM_BASE
#define EMCTABLE_BASE 	 (TZDRAM_ACTUAL_BASE - CARVEOUT_GENFW_SIZE) // before TZDRAM_ACTUAL_BASE

// BL31 Enable IRAM based config.
#define BL31_IRAM_PARAMS           0x4D415249 // "IRAM".
#define BL31_EXTRA_FEATURES_ENABLE 0x52545845 // "EXTR".

// BL31 Disable Secure PMC.
#define PMC_SECURE     0
#define PMC_NON_SECURE 1

// BL31 config.
#define PARAM_EP            0x01
#define PARAM_BL31          0x03
#define PARAM_EP_NON_SECURE	0x1
#define VERSION_1           0x01
#define SPSR_EL2T           0x08

// BL33 config.
#define UBOOT_LOAD_ADDR 0x80110000

typedef struct _param_header {
	u8  type;    // type of the structure.
	u8  version; // version of this structure.
	u16 size;    // size of this structure in bytes.
	u32 attr;    // attributes: unused bits SBZ.
} param_header_t;

typedef struct _aapcs64_params {
	u64 x0;
	u64 x1;
	u64 x2;
	u64 x3;
	u64 x4;
	u64 x5;
	u64 x6;
	u64 x7;
} aapcs64_params_t;

typedef struct _entry_point_info {
	param_header_t h;
	u64 pc;
	u32 spsr;
	u32 align0; // Alignment to u64 for the above member.
	aapcs64_params_t args;
} entry_point_info_t;

typedef struct _image_info {
	param_header_t h;
	u64 image_base; // physical address of base of image.
	u32 image_size; // bytes read from image file.
	u32 image_max_size;
} image_info_t;

typedef struct _bl_v1_params {
	param_header_t h;
	u64 bl31_image_info_ptr;
	u64 bl32_ep_info_ptr;
	u64 bl32_image_info_ptr;
	u64 bl33_ep_info_ptr;
	u64 bl33_image_info_ptr;
} bl_v1_params_t;

typedef struct _plat_params_from_bl2 {
	// TZDRAM.
	u64 tzdram_size;
	u64 tzdram_base;

	s32 uart_id;                  // UART port ID.
	s32 l2_ecc_parity_prot_dis;   // L2 ECC parity protection disable flag.
	u64 boot_profiler_shmem_base; // SHMEM base address for storing the boot logs.

	// SC7 Entry firmware.
	u64 sc7entry_fw_size;
	u64 sc7entry_fw_base;

	s32 enable_ccplex_lock_step;  // Enable dual execution.

	// Enable below features.
	s32 enable_extra_features;

	// MTC table.
	u64 emc_table_size;
	u64 emc_table_base;

	// Initial R2P payload.
	u64 r2p_payload_size;
	u64 r2p_payload_base;

	s32 pmc_security_dis;         // Disable PMC security.
} plat_params_from_bl2_t;

static const u8 retail_pkc_modulus[0x100] = {
	0xF7, 0x86, 0x47, 0xAB, 0x71, 0x89, 0x81, 0xB5, 0xCF, 0x0C, 0xB0, 0xE8, 0x48, 0xA7, 0xFD, 0xAD,
	0xCB, 0x4E, 0x4A, 0x52, 0x0B, 0x1A, 0x8E, 0xDE, 0x41, 0x87, 0x6F, 0xB7, 0x31, 0x05, 0x5F, 0xAA,
	0xEA, 0x97, 0x76, 0x21, 0x20, 0x2B, 0x40, 0x48, 0x76, 0x55, 0x35, 0x03, 0xFE, 0x7F, 0x67, 0x62,
	0xFD, 0x4E, 0xE1, 0x22, 0xF8, 0xF0, 0x97, 0x39, 0xEF, 0xEA, 0x47, 0x89, 0x3C, 0xDB, 0xF0, 0x02,
	0xAD, 0x0C, 0x96, 0xCA, 0x82, 0xAB, 0xB3, 0xCB, 0x98, 0xC8, 0xDC, 0xC6, 0xAC, 0x5C, 0x93, 0x3B,
	0x84, 0x3D, 0x51, 0x91, 0x9E, 0xC1, 0x29, 0x22, 0x95, 0xF0, 0xA1, 0x51, 0xBA, 0xAF, 0x5D, 0xC3,
	0xAB, 0x04, 0x1B, 0x43, 0x61, 0x7D, 0xEA, 0x65, 0x95, 0x24, 0x3C, 0x51, 0x3E, 0x8F, 0xDB, 0xDB,
	0xC1, 0xC4, 0x2D, 0x04, 0x29, 0x5A, 0xD7, 0x34, 0x6B, 0xCC, 0xF1, 0x06, 0xF9, 0xC9, 0xE1, 0xF9,
	0x61, 0x52, 0xE2, 0x05, 0x51, 0xB1, 0x3D, 0x88, 0xF9, 0xA9, 0x27, 0xA5, 0x6F, 0x4D, 0xE7, 0x22,
	0x48, 0xA5, 0xF8, 0x12, 0xA2, 0xC2, 0x5A, 0xA0, 0xBF, 0xC8, 0x76, 0x4B, 0x66, 0xFE, 0x1C, 0x73,
	0x00, 0x29, 0x26, 0xCD, 0x18, 0x4F, 0xC2, 0xB0, 0x51, 0x77, 0x2E, 0x91, 0x09, 0x1B, 0x41, 0x5D,
	0x89, 0x5E, 0xEE, 0x24, 0x22, 0x47, 0xE5, 0xE5, 0xF1, 0x86, 0x99, 0x67, 0x08, 0x28, 0x42, 0xF0,
	0x58, 0x62, 0x54, 0xC6, 0x5B, 0xDC, 0xE6, 0x80, 0x85, 0x6F, 0xE2, 0x72, 0xB9, 0x7E, 0x36, 0x64,
	0x48, 0x85, 0x10, 0xA4, 0x75, 0x38, 0x79, 0x76, 0x8B, 0x51, 0xD5, 0x87, 0xC3, 0x02, 0xC9, 0x1B,
	0x93, 0x22, 0x49, 0xEA, 0xAB, 0xA0, 0xB5, 0xB1, 0x3C, 0x10, 0xC4, 0x71, 0xF0, 0xF1, 0x81, 0x1A,
	0x3A, 0x9C, 0xFC, 0x51, 0x61, 0xB1, 0x4B, 0x18, 0xB2, 0x3D, 0xAA, 0xD6, 0xAC, 0x72, 0x26, 0xB7
};

static const u8 dev_pkc_modulus[0x100] = {
	0x37, 0x84, 0x14, 0xB3, 0x78, 0xA4, 0x7F, 0xD8, 0x71, 0x45, 0xCD, 0x90, 0x51, 0x51, 0xBF, 0x2C,
	0x27, 0x03, 0x30, 0x46, 0xBE, 0x8F, 0x99, 0x3E, 0x9F, 0x36, 0x4D, 0xEB, 0xF7, 0x0E, 0x81, 0x7F,
	0xE4, 0x6B, 0xA8, 0x42, 0x8A, 0xA5, 0x4F, 0x76, 0xCC, 0xCB, 0xC5, 0x31, 0xA8, 0x5A, 0x70, 0x51,
	0x34, 0xBF, 0x1E, 0x8D, 0x6E, 0xCF, 0x05, 0x84, 0xCF, 0x8B, 0xE5, 0x9C, 0x3A, 0xA5, 0xCD, 0x1A,
	0x9C, 0xAC, 0x59, 0x30, 0x09, 0x21, 0x3C, 0xBE, 0x07, 0x5C, 0x8D, 0x1C, 0xD1, 0xA3, 0xC9, 0x8F,
	0x26, 0xE2, 0x99, 0xB2, 0x3C, 0x28, 0xAD, 0x63, 0x0F, 0xF5, 0xA0, 0x1C, 0xA2, 0x34, 0xC4, 0x0E,
	0xDB, 0xD7, 0xE1, 0xA9, 0x5E, 0xE9, 0xA5, 0xA8, 0x64, 0x3A, 0xFC, 0x48, 0xB5, 0x97, 0xDF, 0x55,
	0x7C, 0x9A, 0xD2, 0x8C, 0x32, 0x36, 0x1D, 0xC5, 0xA0, 0xC5, 0x66, 0xDF, 0x8A, 0xAD, 0x76, 0x18,
	0x46, 0x3E, 0xDF, 0xD8, 0xEF, 0xB9, 0xE5, 0xDC, 0xCD, 0x08, 0x59, 0xBC, 0x36, 0x68, 0xD6, 0xFC,
	0x3F, 0xFA, 0x11, 0x00, 0x0D, 0x50, 0xE0, 0x69, 0x0F, 0x70, 0x78, 0x7E, 0xD1, 0xA5, 0x85, 0xCD,
	0x13, 0xBC, 0x42, 0x74, 0x33, 0x0C, 0x11, 0x24, 0x1E, 0x33, 0xD5, 0x31, 0xB7, 0x3E, 0x48, 0x94,
	0xCC, 0x81, 0x29, 0x1E, 0xB1, 0xCF, 0x4C, 0x36, 0x7F, 0xE1, 0x1C, 0x15, 0xD4, 0x3F, 0xFB, 0x12,
	0xC2, 0x73, 0x22, 0x16, 0x52, 0xE0, 0x5C, 0x4C, 0x94, 0xE0, 0x87, 0x47, 0xEA, 0xD0, 0x9F, 0x42,
	0x9B, 0xAC, 0xB6, 0xB5, 0xB6, 0x34, 0xE4, 0x55, 0x49, 0xD7, 0xC0, 0xAE, 0xD4, 0x22, 0xB3, 0x5C,
	0x87, 0x64, 0x42, 0xEC, 0x11, 0x6D, 0xBC, 0x09, 0xC0, 0x80, 0x07, 0xD0, 0xBD, 0xBA, 0x45, 0xFE,
	0xD5, 0x52, 0xDA, 0xEC, 0x41, 0xA4, 0xAD, 0x7B, 0x36, 0x86, 0x18, 0xB4, 0x5B, 0xD1, 0x30, 0xBB
};

static int _atf_sd_read_load(u32 load_address, const char *path, u32 *fsize)
{
	FIL fp;
	void *buf = (void *)load_address;

	if (f_open(&fp, path, FA_READ) != FR_OK)
		return 1;

	u32 size = f_size(&fp);
	if (fsize)
		*fsize = size;

	if (f_read(&fp, buf, size, NULL) != FR_OK)
	{
		f_close(&fp);

		return 1;
	}

	f_close(&fp);

	return 0;
}

static void _mc_config_l4t_carveout()
{
	u32 carveout_base = TZDRAM_ACTUAL_BASE;

	// Empty initial VPR carveout. Kernel instructs TZ to set it for CMA.
	MC(MC_VIDEO_PROTECT_BOM) = 0;
	MC(MC_VIDEO_PROTECT_SIZE_MB) = 0;
	MC(MC_VIDEO_PROTECT_GPU_OVERRIDE_0) = 0x2A800000; // HOS: 1
    MC(MC_VIDEO_PROTECT_GPU_OVERRIDE_1) = 2;          // HOS: 0
	MC(MC_VIDEO_PROTECT_REG_CTRL) = VPR_CTRL_TZ_SECURE | VPR_CTRL_LOCKED;

	// Empty TSEC carveout.
	MC(MC_SEC_CARVEOUT_BOM) = 0xFFF00000;
	MC(MC_SEC_CARVEOUT_SIZE_MB) = 0;
	MC(MC_SEC_CARVEOUT_REG_CTRL) = 0;

	// Empty CPU Microcode carveout.
	MC(MC_MTS_CARVEOUT_BOM) = 0xFFF00000;
	MC(MC_MTS_CARVEOUT_SIZE_MB) = 0;
	MC(MC_MTS_CARVEOUT_REG_CTRL) = 0;

	// Configure generalized security carveouts.
#if CARVEOUT_NVDEC_ENABLE
	// Set NVDEC carveout. Only for NVDEC bl/prod.
	carveout_base -= ALIGN(CARVEOUT_NVDEC_SIZE, SZ_1M);
	MC(MC_SECURITY_CARVEOUT1_BOM) = carveout_base;
	MC(MC_SECURITY_CARVEOUT1_BOM_HI) = 0;
	MC(MC_SECURITY_CARVEOUT1_SIZE_128KB) = CARVEOUT_NVDEC_SIZE / SZ_128K;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS2) = SEC_CARVEOUT_CA2_R_TSEC  | SEC_CARVEOUT_CA2_W_TSEC;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS3) = SEC_CARVEOUT_CA3_R_NVDEC | SEC_CARVEOUT_CA3_W_NVDEC;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT1_CFG0) = SEC_CARVEOUT_CFG_LOCKED            |
									 SEC_CARVEOUT_CFG_UNTRANSLATED_ONLY |
									 SEC_CARVEOUT_CFG_RD_UNK            |
									 SEC_CARVEOUT_CFG_RD_FALCON_LS      |
									 SEC_CARVEOUT_CFG_RD_FALCON_HS      |
									 SEC_CARVEOUT_CFG_WR_FALCON_HS      |
									 SEC_CARVEOUT_CFG_APERTURE_ID(1)    |
									 SEC_CARVEOUT_CFG_FORCE_APERTURE_ID_MATCH;
	DPRINTF("GSC1: NVDEC Carveout: %08X - %08X\n",
		MC(MC_SECURITY_CARVEOUT1_BOM), MC(MC_SECURITY_CARVEOUT1_BOM) + MC(MC_SECURITY_CARVEOUT1_SIZE_128KB) * SZ_128K);
#elif defined(CARVEOUT_GENFW_ENABLE)
	// Set MTC Table carveout.
	// No need to decrement carveout base as next one will use 1MB aligned base with 256KB size.
	MC(MC_SECURITY_CARVEOUT1_BOM) = EMCTABLE_BASE;
	MC(MC_SECURITY_CARVEOUT1_BOM_HI) = 0x0;
	MC(MC_SECURITY_CARVEOUT1_SIZE_128KB) = CARVEOUT_GENFW_SIZE / SZ_128K;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS0) = SEC_CARVEOUT_CA0_R_BPMP_C;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS1) = SEC_CARVEOUT_CA1_W_BPMP_C     |
											   SEC_CARVEOUT_CA1_R_CCPLEX_C   |
											   SEC_CARVEOUT_CA1_R_CCPLEXLP_C |
											   SEC_CARVEOUT_CA1_W_CCPLEX_C   |
											   SEC_CARVEOUT_CA1_W_CCPLEXLP_C;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT1_CLIENT_FORCE_INTERNAL_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT1_CFG0) = SEC_CARVEOUT_CFG_RD_ALL |
									 SEC_CARVEOUT_CFG_WR_ALL |
									 SEC_CARVEOUT_CFG_LOCKED;
	DPRINTF("GSC1: MTCTB Carveout: %08X - %08X\n",
		MC(MC_SECURITY_CARVEOUT1_BOM), MC(MC_SECURITY_CARVEOUT1_BOM) + MC(MC_SECURITY_CARVEOUT1_SIZE_128KB) * SZ_128K);
#endif

	// Set GPU FW WPR carveout.
	carveout_base -= ALIGN(CARVEOUT_GPUFW_SIZE, SZ_1M);

	// Sanitize it and enable GSC3 for ACR.
	memset((void *)carveout_base, 0, CARVEOUT_GPUFW_SIZE);
	*(vu32 *)(carveout_base + CARVEOUT_GPUFW_SIZE - 4) = ACR_GSC3_ENABLE_MAGIC;
	bpmp_mmu_maintenance(BPMP_MMU_MAINT_INVALID_WAY, false);

	// Set carveout config.
	MC(MC_SECURITY_CARVEOUT2_BOM) = carveout_base;
	MC(MC_SECURITY_CARVEOUT2_BOM_HI) = 0;
	MC(MC_SECURITY_CARVEOUT2_SIZE_128KB) = CARVEOUT_GPUFW_SIZE / SZ_128K;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_ACCESS2) = SEC_CARVEOUT_CA2_R_GPU  | SEC_CARVEOUT_CA2_W_GPU;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_ACCESS4) = SEC_CARVEOUT_CA4_R_GPU2 | SEC_CARVEOUT_CA4_W_GPU2;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_FORCE_INTERNAL_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_FORCE_INTERNAL_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_FORCE_INTERNAL_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_FORCE_INTERNAL_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT2_CLIENT_FORCE_INTERNAL_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT2_CFG0) = SEC_CARVEOUT_CFG_LOCKED            |
									 SEC_CARVEOUT_CFG_UNTRANSLATED_ONLY |
									 SEC_CARVEOUT_CFG_RD_ALL            |
									 SEC_CARVEOUT_CFG_RD_UNK            |
									 SEC_CARVEOUT_CFG_RD_FALCON_LS      |
									 SEC_CARVEOUT_CFG_RD_FALCON_HS      |
									 SEC_CARVEOUT_CFG_WR_FALCON_LS      |
									 SEC_CARVEOUT_CFG_WR_FALCON_HS      |
									 SEC_CARVEOUT_CFG_APERTURE_ID(2)    |
									 SEC_CARVEOUT_CFG_SEND_CFG_TO_GPU   |
									 SEC_CARVEOUT_CFG_FORCE_APERTURE_ID_MATCH; // SEC_CARVEOUT_CFG_IS_WPR is set from GPU.
	DPRINTF("GSC2: GPUFW Carveout: %08X - %08X\n",
		MC(MC_SECURITY_CARVEOUT2_BOM), MC(MC_SECURITY_CARVEOUT2_BOM) + MC(MC_SECURITY_CARVEOUT2_SIZE_128KB) * SZ_128K);

	/*
	 * Set GPU WPR carveout.
	 *
	 * L4T: SZ_2M or SZ_13M. On non SecureOS env, only 0x100 bytes are used.
	 * On 4GB+ systems, it gets set at BANK1_TOP - SIZE;
	 */
	MC(MC_SECURITY_CARVEOUT3_BOM) = carveout_base + CARVEOUT_GPUFW_SIZE;
	MC(MC_SECURITY_CARVEOUT3_BOM_HI) = 0x0;
	MC(MC_SECURITY_CARVEOUT3_SIZE_128KB) = CARVEOUT_GPUWPR_SIZE / SZ_128K;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS2) = 0; // HOS: SEC_CARVEOUT_CA2_R_GPU  | SEC_CARVEOUT_CA2_W_GPU
	MC(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS4) = 0; // HOS: SEC_CARVEOUT_CA4_R_GPU2 | SEC_CARVEOUT_CA4_W_GPU2
	MC(MC_SECURITY_CARVEOUT3_CLIENT_FORCE_INTERNAL_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_FORCE_INTERNAL_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_FORCE_INTERNAL_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_FORCE_INTERNAL_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT3_CLIENT_FORCE_INTERNAL_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT3_CFG0) = SEC_CARVEOUT_CFG_LOCKED            |
									 SEC_CARVEOUT_CFG_UNTRANSLATED_ONLY |
									 SEC_CARVEOUT_CFG_RD_ALL            |
									 SEC_CARVEOUT_CFG_RD_UNK            |
									 SEC_CARVEOUT_CFG_RD_FALCON_LS      |
									 SEC_CARVEOUT_CFG_RD_FALCON_HS      |
									 SEC_CARVEOUT_CFG_WR_FALCON_LS      |
									 SEC_CARVEOUT_CFG_WR_FALCON_HS      |
									 SEC_CARVEOUT_CFG_APERTURE_ID(3)    |
									 SEC_CARVEOUT_CFG_SEND_CFG_TO_GPU   |
									 SEC_CARVEOUT_CFG_FORCE_APERTURE_ID_MATCH; // SEC_CARVEOUT_CFG_IS_WPR is set from GPU.
	DPRINTF("GSC3: GPUW2 Carveout: %08X - %08X\n",
		MC(MC_SECURITY_CARVEOUT3_BOM), MC(MC_SECURITY_CARVEOUT3_BOM) + MC(MC_SECURITY_CARVEOUT3_SIZE_128KB) * SZ_128K);

	// Set TSECA/B carveout. Only for NVDEC bl/prod and TSEC.
	carveout_base -= CARVEOUT_TSEC_ENABLE ? ALIGN(CARVEOUT_TSEC_SIZE, SZ_1M) : 0;
	MC(MC_SECURITY_CARVEOUT4_BOM) = CARVEOUT_TSEC_ENABLE ? carveout_base : 0;
	MC(MC_SECURITY_CARVEOUT4_BOM_HI) = 0x0;
	MC(MC_SECURITY_CARVEOUT4_SIZE_128KB) = CARVEOUT_TSEC_ENABLE ? CARVEOUT_TSEC_SIZE / SZ_128K : 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_ACCESS2) = SEC_CARVEOUT_CA2_R_TSEC  | SEC_CARVEOUT_CA2_W_TSEC;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_ACCESS4) = SEC_CARVEOUT_CA4_R_TSECB | SEC_CARVEOUT_CA4_W_TSECB;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_FORCE_INTERNAL_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_FORCE_INTERNAL_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_FORCE_INTERNAL_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_FORCE_INTERNAL_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT4_CLIENT_FORCE_INTERNAL_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT4_CFG0) = SEC_CARVEOUT_CFG_LOCKED         |
									 SEC_CARVEOUT_CFG_RD_FALCON_HS   |
									 SEC_CARVEOUT_CFG_WR_FALCON_HS   |
									 SEC_CARVEOUT_CFG_APERTURE_ID(4) |
									 SEC_CARVEOUT_CFG_FORCE_APERTURE_ID_MATCH;
	DPRINTF("GSC4: TSEC1 Carveout: %08X - %08X\n",
		MC(MC_SECURITY_CARVEOUT4_BOM), MC(MC_SECURITY_CARVEOUT4_BOM) + MC(MC_SECURITY_CARVEOUT4_SIZE_128KB) * SZ_128K);

	// Set TSECA carveout. Only for NVDEC bl/prod and TSEC.
	carveout_base -= CARVEOUT_TSEC_ENABLE ? ALIGN(CARVEOUT_TSEC_SIZE, SZ_1M) : 0;
	MC(MC_SECURITY_CARVEOUT5_BOM) = CARVEOUT_TSEC_ENABLE ? carveout_base : 0;
	MC(MC_SECURITY_CARVEOUT5_BOM_HI) = 0;
	MC(MC_SECURITY_CARVEOUT5_SIZE_128KB) = CARVEOUT_TSEC_ENABLE ? CARVEOUT_TSEC_SIZE / SZ_128K : 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_ACCESS2) = SEC_CARVEOUT_CA2_R_TSEC  | SEC_CARVEOUT_CA2_W_TSEC;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_FORCE_INTERNAL_ACCESS0) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_FORCE_INTERNAL_ACCESS1) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_FORCE_INTERNAL_ACCESS2) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_FORCE_INTERNAL_ACCESS3) = 0;
	MC(MC_SECURITY_CARVEOUT5_CLIENT_FORCE_INTERNAL_ACCESS4) = 0;
	MC(MC_SECURITY_CARVEOUT5_CFG0) = SEC_CARVEOUT_CFG_LOCKED         |
									 SEC_CARVEOUT_CFG_RD_FALCON_HS   |
									 SEC_CARVEOUT_CFG_WR_FALCON_HS   |
									 SEC_CARVEOUT_CFG_APERTURE_ID(5) |
									 SEC_CARVEOUT_CFG_FORCE_APERTURE_ID_MATCH;
	DPRINTF("GSC5: TSEC2 Carveout: %08X - %08X\n",
		MC(MC_SECURITY_CARVEOUT5_BOM), MC(MC_SECURITY_CARVEOUT5_BOM) + MC(MC_SECURITY_CARVEOUT5_SIZE_128KB) * SZ_128K);

	DPRINTF("MEM Bank 0 TOP %08X\n", carveout_base);

#if 0 // Following registers can only be written by CCPLEX. Nothing to do here.
	// Set TZDRAM carveout.
	MC(MC_SECURITY_CFG0) = TZDRAM_ACTUAL_BASE;
	MC(MC_SECURITY_CFG1) = TZDRAM_ACTUAL_SIZE / SZ_1M;
#endif
}

static void _sdram_lp0_save_l4t_params()
{
	struct tegra_pmc_regs *pmc = (struct tegra_pmc_regs *)PMC_BASE;

#define pack(src, src_bits, dst, dst_bits) { \
		u32 mask = 0xffffffff >> (31 - ((1 ? src_bits) - (0 ? src_bits))); \
		dst &= ~(mask << (0 ? dst_bits)); \
		dst |= ((src >> (0 ? src_bits)) & mask) << (0 ? dst_bits); }

#define s(param, src_bits, pmcreg, dst_bits) \
	pack(MC(param), src_bits, pmc->pmcreg, dst_bits)

// 32 bits version of s macro.
#define s32(param, pmcreg) pmc->pmcreg = MC(param)

	// Only save changed carveout registers into PMC for SC7 Exit.

	// VPR.
	s(MC_VIDEO_PROTECT_BOM, 31:20, secure_scratch52, 26:15);
	s(MC_VIDEO_PROTECT_SIZE_MB, 11:0, secure_scratch53, 11:0);
	s(MC_VIDEO_PROTECT_REG_CTRL, 1:0, secure_scratch13, 31:30);
	s32(MC_VIDEO_PROTECT_GPU_OVERRIDE_0, secure_scratch12);
	s(MC_VIDEO_PROTECT_GPU_OVERRIDE_1, 15:0, secure_scratch49, 15:0);

	// SEC.
	s(MC_SEC_CARVEOUT_BOM, 31:20, secure_scratch53, 23:12);
	s(MC_SEC_CARVEOUT_SIZE_MB, 11:0, secure_scratch54, 11:0);
	s(MC_SEC_CARVEOUT_REG_CTRL, 0:0, secure_scratch18, 30:30);

	// MTS.
	s(MC_MTS_CARVEOUT_BOM, 31:20, secure_scratch54, 23:12);
	s(MC_MTS_CARVEOUT_SIZE_MB, 11:0, secure_scratch55, 11:0);
	s(MC_MTS_CARVEOUT_REG_CTRL, 0:0, secure_scratch18, 31:31);

	// NVDEC or MTCTB.
	s(MC_SECURITY_CARVEOUT1_BOM, 31:17, secure_scratch50, 29:15);
	s(MC_SECURITY_CARVEOUT1_SIZE_128KB, 11:0, secure_scratch57, 11:0);
	s32(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS0, secure_scratch59);
	s32(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS1, secure_scratch60);
	s32(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS2, secure_scratch61);
	s32(MC_SECURITY_CARVEOUT1_CLIENT_ACCESS3, secure_scratch62);
	s(MC_SECURITY_CARVEOUT1_CFG0, 2:0, secure_scratch56, 27:25);
	s(MC_SECURITY_CARVEOUT1_CFG0, 6:3, secure_scratch41, 28:25);
	s(MC_SECURITY_CARVEOUT1_CFG0, 13:7, secure_scratch42, 31:25);
	s(MC_SECURITY_CARVEOUT1_CFG0, 17:14, secure_scratch43, 28:25);
	s(MC_SECURITY_CARVEOUT1_CFG0, 21:18, secure_scratch44, 28:25);
	s(MC_SECURITY_CARVEOUT1_CFG0, 25:22, secure_scratch56, 31:28);
	s(MC_SECURITY_CARVEOUT1_CFG0, 26:26, secure_scratch57, 24:24);

	// GPU FW.
	s(MC_SECURITY_CARVEOUT2_BOM, 31:17, secure_scratch51, 29:15);
	s(MC_SECURITY_CARVEOUT2_SIZE_128KB, 11:0, secure_scratch56, 23:12);
	s(MC_SECURITY_CARVEOUT2_CFG0, 2:0, secure_scratch55, 27:25);
	s(MC_SECURITY_CARVEOUT2_CFG0, 6:3, secure_scratch19, 31:28);
	s(MC_SECURITY_CARVEOUT2_CFG0, 10:7, secure_scratch20, 31:28);
	s(MC_SECURITY_CARVEOUT2_CFG0, 13:11, secure_scratch41, 31:29);
	s(MC_SECURITY_CARVEOUT2_CFG0, 17:14, secure_scratch39, 30:27);
	s(MC_SECURITY_CARVEOUT2_CFG0, 21:18, secure_scratch40, 30:27);
	s(MC_SECURITY_CARVEOUT2_CFG0, 25:22, secure_scratch55, 31:28);
	s(MC_SECURITY_CARVEOUT2_CFG0, 26:26, secure_scratch56, 24:24);

	// GPU WPR.
	s(MC_SECURITY_CARVEOUT3_BOM, 31:17, secure_scratch50, 14:0);
	s(MC_SECURITY_CARVEOUT3_SIZE_128KB, 11:0, secure_scratch56, 11:0);
	s32(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS2, secure_scratch71);
	s32(MC_SECURITY_CARVEOUT3_CLIENT_ACCESS3, secure_scratch73);
	s(MC_SECURITY_CARVEOUT3_CFG0, 2:0, secure_scratch57, 27:25);
	s(MC_SECURITY_CARVEOUT3_CFG0, 10:3, secure_scratch47, 29:22);
	s(MC_SECURITY_CARVEOUT3_CFG0, 13:11, secure_scratch43, 31:29);
	s(MC_SECURITY_CARVEOUT3_CFG0, 21:14, secure_scratch48, 29:22);
	s(MC_SECURITY_CARVEOUT3_CFG0, 25:22, secure_scratch57, 31:28);
	s(MC_SECURITY_CARVEOUT3_CFG0, 26:26, secure_scratch58, 0:0);

	// TSECA.
	s(MC_SECURITY_CARVEOUT4_BOM, 31:17, secure_scratch51, 14:0);
	s(MC_SECURITY_CARVEOUT4_SIZE_128KB, 11:0, secure_scratch55, 23:12);
	s(MC_SECURITY_CARVEOUT4_CFG0, 26:0, secure_scratch39, 26:0);

	// TSECB.
	s(MC_SECURITY_CARVEOUT5_BOM, 31:17, secure_scratch52, 14:0);
	s(MC_SECURITY_CARVEOUT5_SIZE_128KB, 11:0, secure_scratch57, 23:12);
	s(MC_SECURITY_CARVEOUT5_CFG0, 26:0, secure_scratch40, 26:0);
}

void load_atf(void)
{
	static bl_v1_params_t bl_v1_params;
	static plat_params_from_bl2_t plat_params;
	static entry_point_info_t bl33_ep_info;
	u32 sc7entry_size = 0;
	emc_table_t *mtc_table;

	// Only supported on T210 for now.
	if (hw_get_chip_id() != GP_HIDREV_MAJOR_T210)
		return;

#ifdef DEBUG_HEKATF
	gfx_con_setpos(0, 0);
#endif

	memset(&bl_v1_params, 0, sizeof(bl_v1_params_t));
	memset(&bl33_ep_info, 0, sizeof(entry_point_info_t));
	memset(&plat_params, 0, sizeof(plat_params_from_bl2_t));

	// Get MTC table.
	mtc_table = minerva_get_mtc_table();
	if (!mtc_table)
		return;

	if (!sd_mount())
		return;

	if (_atf_sd_read_load(TZDRAM_BASE,     "switchroot/ubuntu/boot/bl31.bin", NULL))
		return;

	if (_atf_sd_read_load(UBOOT_LOAD_ADDR, "switchroot/ubuntu/boot/uboot.bin", NULL))
		return;

	if (_atf_sd_read_load(SC7ENTRY_BASE,   "switchroot/ubuntu/boot/sc7entry.fw", &sc7entry_size))
		return;

	if (_atf_sd_read_load(SC7EXIT_BASE,    "switchroot/ubuntu/boot/sc7exit.fw", NULL))
		return;

	// Done loading bootloaders/firmware.
	sd_end();

	// Apply Nintendo Switch RSA modulus to SC7Exit firmware.
	if (fuse_read_hw_state() == FUSE_NX_HW_STATE_PROD)
		memcpy((void *)(SC7EXIT_BASE + 0x10), retail_pkc_modulus, 0x100);
	else
		memcpy((void *)(SC7EXIT_BASE + 0x10), dev_pkc_modulus, 0x100);

	// Set SC7Exit firmware address to PMC for bootrom to find it.
	PMC(APBDEV_PMC_SCRATCH1) = SC7EXIT_BASE;

	// Set carveouts and save them to PMC for SC7 Exit.
	_mc_config_l4t_carveout();
	_sdram_lp0_save_l4t_params();

	// Set BL33 params.
	bl33_ep_info.h.type = PARAM_EP;
	bl33_ep_info.h.version = VERSION_1;
	bl33_ep_info.h.size = sizeof(entry_point_info_t);
	bl33_ep_info.h.attr = PARAM_EP_NON_SECURE;
	bl33_ep_info.pc = UBOOT_LOAD_ADDR;
	bl33_ep_info.spsr = SPSR_EL2T;

	// Set BL31 params.
	bl_v1_params.h.type = PARAM_BL31;
	bl_v1_params.h.version = VERSION_1;
	bl_v1_params.h.size = sizeof(bl_v1_params_t);
	bl_v1_params.h.attr = 0x00;
	bl_v1_params.bl33_ep_info_ptr = (u64)(u32)&bl33_ep_info;

	// Set params address to PMC.
	PMC(APBDEV_PMC_SECURE_SCRATCH108) = (u32)&bl_v1_params;

	// Set Platform parameters.
	plat_params.uart_id = 2; // UART_B. TODO: UART_B, take from hekate flag? Read from config?
	plat_params.tzdram_base = TZDRAM_BASE;
	plat_params.tzdram_size = TZDRAM_SIZE;

	// Enable below features.
	plat_params.enable_extra_features = BL31_EXTRA_FEATURES_ENABLE;

	// Set sc7entry fw and emc table parameters.
	plat_params.sc7entry_fw_base = SC7EXIT_HDR_BASE;
	plat_params.sc7entry_fw_size = ALIGN(sc7entry_size + SC7ENTRY_HDR_SIZE, SZ_PAGE);
	plat_params.emc_table_base = EMCTABLE_BASE; // Put below carveouts and TZDRAM.
	plat_params.emc_table_size = sizeof(emc_table_t) * 10;

	// Set PMC access security.
	plat_params.pmc_security_dis = PMC_NON_SECURE;

	// Set initial R2P payload.
	volatile reloc_meta_t *reloc = (reloc_meta_t *)(IPL_LOAD_ADDR + 0x7C);
	memcpy((u8 *)PAYLOAD_BASE, (u8 *)reloc->start, reloc->end - reloc->start);
	memset((u8 *)PAYLOAD_BASE + 0x94, 0, sizeof(boot_cfg_t)); // Clear Boot Config Storage.
	plat_params.r2p_payload_base = PAYLOAD_BASE;
	plat_params.r2p_payload_size = ALIGN(reloc->end - reloc->start, SZ_PAGE);

	// Set and enable IRAM based BL31 config.
	PMC(APBDEV_PMC_SECURE_SCRATCH109) = (u32)&plat_params;
	PMC(APBDEV_PMC_SECURE_SCRATCH110) = BL31_IRAM_PARAMS;

#ifdef DEBUG_HEKATF
	DPRINTF("\nPress any key\n");
	btn_wait();
#endif

	// RAM training is access blocking. So turn off backlight to hide display memfetch glitches.
	display_backlight_brightness(0, 1000);

	// Train the rest of the table, apply FSP WAR, set RAM to 800 MHz and copy it.
	minerva_prep_boot_l4t();
	memcpy((u32 *)EMCTABLE_BASE, mtc_table, sizeof(emc_table_t) * 10);

	// Deinit HW we don't want and let display on.
	hw_reinit_workaround(true, BL_MAGIC_HEKATF_SLD);
	display_backlight_brightness(h_cfg.backlight, 1000);

	// Turn on PLLC to default frequency.
	clock_enable_pllc(53); // VCO/OUT0: 508.8 MHz.

	// Reset System Counters.
	SYSCTR0(SYSCTR0_COUNTERID0) = 0;
	SYSCTR0(SYSCTR0_COUNTERID1) = 0;
	SYSCTR0(SYSCTR0_COUNTERID2) = 0;
	SYSCTR0(SYSCTR0_COUNTERID3) = 0;
	SYSCTR0(SYSCTR0_COUNTERID4) = 0;
	SYSCTR0(SYSCTR0_COUNTERID5) = 0;
	SYSCTR0(SYSCTR0_COUNTERID6) = 0;
	SYSCTR0(SYSCTR0_COUNTERID7) = 0;
	SYSCTR0(SYSCTR0_COUNTERID8) = 0;
	SYSCTR0(SYSCTR0_COUNTERID9) = 0;
	SYSCTR0(SYSCTR0_COUNTERID10) = 0;
	SYSCTR0(SYSCTR0_COUNTERID11) = 0;

	// Clear any MC error.
	MC(MC_INTSTATUS) = MC(MC_INTSTATUS);

	// Wait for SD pads to power down completely.
	sdmmc_storage_init_wait_sd();

	// Launch ATF.
	ccplex_boot_cpu0(TZDRAM_BASE);

	// Halt BPMP.
	while (true)
		bpmp_halt();
}
